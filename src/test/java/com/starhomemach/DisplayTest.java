package com.starhomemach;
/**
 * Show given number on a 7-segments display
 * i.e.
 * for 0
 * ' - '
 * '| |'
 *
 * '| |'
 * ' - '
 * for 1
 * '   '
 * '  |'
 * '   '
 * '  |'
 * '   '
 * for 8
 * ' - '
 * '| |'
 * ' - '
 * '| |'
 * ' - '
 * digits in a number separated with space,
 * i.e. for 13
 * '     - '
 * '  |   |'
 * '     - '
 * '  |   |'
 * '     - '
 */
public class DisplayTest {
}
