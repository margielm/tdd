package com.starhomemach;

import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    @Test
    public void shouldAdd2And2() {
        //given
        Calculator calculator = new Calculator();

        //when
        int result = calculator.add(2, 2);

        //then
        assertEquals(4, result);
    }
}
